mkdir -p target/linux/x86/64/patches-5.4
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/690-mptcp_trunk.patch' -o target/linux/x86/64/patches-5.4/690-mptcp_trunk.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/692-tcp_nanqinlang.patch' -o target/linux/x86/64/patches-5.4/692-tcp_nanqinlang.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/693-tcp_bbr2.patch' -o target/linux/x86/64/patches-5.4/693-tcp_bbr2.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/952-net-conntrack-events-support-multiple-registrant.patch' -o target/linux/x86/64/patches-5.4/952-net-conntrack-events-support-multiple-registrant.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/953-net-patch-linux-kernel-to-support-shortcut-fe.patch' -o target/linux/x86/64/patches-5.4/953-net-patch-linux-kernel-to-support-shortcut-fe.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/998-ndpi-netfilter.patch' -o target/linux/x86/64/patches-5.4/998-ndpi-netfilter.patch && \
curl 'https://raw.githubusercontent.com/Ysurac/openmptcprouter/develop/root/target/linux/generic/hack-5.4/999-stop-promiscuous-info.patch' -o target/linux/x86/64/patches-5.4/999-stop-promiscuous-info.patch

